module.exports = function(grunt) {
    'use strict';

    var path = require('path');

    var config = {
        // path to task.js files, defaults to grunt dir
        configPath: path.join(process.cwd(), 'grunt'),

        // auto grunt.initConfig
        init: true,

        // data passed into config.  Can use with <%= test %>
        data: {
            notify_hooks: {
                options: {
                    enabled: true,
                    max_jshint_notifications: 5, // maximum number of notifications from jshint output
                    title: "Flybe web-app", // defaults to the name in package.json, or will use project directory's name
                    success: true, // whether successful grunt executions should be notified automatically
                    duration: 3 // the duration of notification in seconds, for `notify-send only
                }
            }
        },

        // can optionally pass options to load-grunt-tasks.
        // If you set to false, it will disable auto loading tasks.
        loadGruntTasks: {
            pattern: 'grunt-*',
            config: require('./package.json'),
            scope: 'devDependencies'
        },

        //can post process config object before it gets passed to grunt
        postProcess: function(config) {},

        //allows to manipulate the config object before it gets merged with the data object
        preMerge: function(config, data) {}
    };

    // Force use of Unix newlines
    grunt.util.linefeed = '\n';

    // Measures the time each task takes
    require('time-grunt')(grunt);

    // load grunt config
    require('load-grunt-config')(grunt, config);

    // This is required if options are required for grunt-notify.
    grunt.task.run('notify_hooks');
};
