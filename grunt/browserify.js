module.exports = {

    dev: {

        options: {
            debug : true,
            transform: [ require('grunt-react').browserify ]
        },
        files: {
            'dist/app.js': [
                'react_components/*.jsx' ,'react_components/myaccount/*.jsx', 'react_components/manage/*.jsx'
            ]

        }
    }
}