var React = require('react');

module.exports = React.createClass({

    getInitialState: function(){
        return {

            buttons: ["7","8","9","4","5","6",
                "3","2","1","0",".","+","="],
            result: 0
        }
    },

    render: function(){

        var self = this;

        var ListButtons = this.state.buttons.map(function(btn) {
            return <div className="three columns"><input className={self.setButtonStyles(btn)} type="button" onClick={self.calculate.bind(self,btn)} value={btn} /></div>
        });

        return (
            <div>
                <div className="row">
                    <input type="text" name="txtResult" ref="txtResult" value={this.state.result} className="eleven columns"/>
                </div>
                <div className="row">{ListButtons}</div>
                <div className="row">
                    <input onClick={this.clearScreen} type="button" className="ten columns" value='Clear'/>
                </div>
            </div>
        )
    },

    componentDidMount: function(){
        console.log('The calculator has mounted');
    },

    clearScreen: function(){
        this.setState({result:0});
    },

    //TODO - set button styles
    setButtonStyles: function(btn){

        var operands = ['+','='];

        if (operands.indexOf(btn)>-1 ){
            return ' operand'
        }
    },

    calculate:function(buttonPressed){

        var operands = ['+','='];

        if (buttonPressed === '=') {
            this.setState({result:eval(this.state.result)});
        }
        else {
            if (this.state.result == 0 ){
                if (operands.indexOf(buttonPressed)>-1){
                    this.setState({result: 0})
                }
                else {
                    this.setState({result:buttonPressed})
                }
            }
            else {
                this.setState({result:this.state.result+buttonPressed});
            }
        }

    }
});