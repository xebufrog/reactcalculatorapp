var React = require('react'),
    Calculator = require('./Calculator.jsx');

React.render(
    <Calculator />,
    document.getElementById('calc')
);